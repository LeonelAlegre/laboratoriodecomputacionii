package Desafios;
import java.util.Scanner;


public class Desafio9 {
  public static double calcularPesoIdeal(char genero, int alt){
        double peso = 0;
        if (genero == 'm'){
           peso = alt - 120;
        }else{
            peso = alt - 110;
        }
        return peso;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int alt;
        char genero;
        int peso;
        
        System.out.println("Ingrese su sexo M (mujer) o H (hombre)");
        genero = sc.next().charAt(0);
        Character.toLowerCase(genero);
        
        switch (genero){
            case 'M':
                System.out.println("Ingrese su altura: ");
                alt = sc.nextInt();
                System.out.println("El peso ideal seria: "+calcularPesoIdeal(genero, alt));
                break;
            case 'H':
                System.out.println("Ingrese su altura: ");
                alt = sc.nextInt();
                System.out.println("El peso ideal seria: "+calcularPesoIdeal(genero, alt));
                break;
            default:
                System.out.println("Ingrese un genero correcto");
        }
    
    }


}