package Desafios;
import java.util.*;

public class Desafio10 {
	public static Scanner entrada=new Scanner(System.in);
 
	public static void main(String[] args) {
		System.out.println("Juego de adivinar un numero");
		System.out.println("---------------------------");
 
		
		int numero=(int)(Math.random()*100);
 
		System.out.println("\nIndica un numero entre el 0 y el 100");
 
		int entrada=-1;
		int contador=0;
 
		
		do {
 
			
			entrada=obtenerValor();
			if(entrada>numero) {
				System.out.println("\nEl numero es menor");
			}else if(entrada<numero) {
				System.out.println("\nEl numero es mayor");
			}
			contador++;
		} while(entrada!=numero);
 
		System.out.println("\nHas avariguado el numero en " + contador + " intentos.");
	}
 
	public static int obtenerValor() {
		int valor=0;
 
		while(true){
			try {
				System.out.print("\nIndica el numero: ");
				valor=entrada.nextInt();
 
				if(valor<0 || valor>100) {
					System.out.println("\nEl numero tiene que estar entre el 0 y el 100");
				}else{
		
					break;
				}
			}catch(InputMismatchException e) {
				
				System.out.println("\nEl valor tiene que ser numerico...");
				entrada.nextLine();
			}
		}
		return valor;
	}
 
}